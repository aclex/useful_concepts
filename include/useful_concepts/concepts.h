#ifndef USEFUL_CONCEPTS_CONCEPTS_H
#define USEFUL_CONCEPTS_CONCEPTS_H

#include <concepts>

namespace useful_concepts
{
	template<typename T, typename U> concept decayed_as = std::same_as<std::decay_t<T>, std::decay_t<U>>;
	template<typename T, typename U> concept cvref_of = std::same_as<std::remove_cvref_t<T>, U>;
	template<typename T> concept arithmetic = std::is_arithmetic_v<T>;
}

#endif // USEFUL_CONCEPTS_CONCEPTS_H
