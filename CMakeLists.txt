cmake_minimum_required(VERSION 3.13)

if (CMAKE_VERSION VERSION_LESS "3.21")
	if(NOT DEFINED PROJECT_NAME)
		set(PROJECT_IS_TOP_LEVEL TRUE)
	else()
		set(PROJECT_IS_TOP_LEVEL FALSE)
	endif()
endif()

project(useful_concepts LANGUAGES CXX)

include(CTest)
include(GNUInstallDirs)

add_library(${PROJECT_NAME} INTERFACE)
target_include_directories(${PROJECT_NAME}
	INTERFACE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
	INTERFACE $<INSTALL_INTERFACE:${CMAKE_FULL_INSTALL_INCLUDEDIR}>
	)
target_compile_features(${PROJECT_NAME} INTERFACE cxx_std_20)

if (NOT PROJECT_IS_TOP_LEVEL)
	set_target_properties(${PROJECT_NAME} PROPERTIES IMPORTED TRUE)
endif ()

if(BUILD_TESTING)
	add_subdirectory(test)
endif()
