#include <concepts>
#include <iomanip>
#include <iostream>
#include <stdexcept>

#include "useful_concepts/concepts.h"

using namespace std;

using namespace useful_concepts;

namespace
{
	struct A{};

	struct B : A{};

	void foo(cvref_of<A> auto&& a)
	{
		cout << "foo(auto): " << __PRETTY_FUNCTION__ << endl;
	}

	template<typename D> void foo(D&& d)
	{
		cout << "foo<>(): " << __PRETTY_FUNCTION__ << endl;
		throw runtime_error("Shouldn't have gone here.");
	}

	template<typename T> void template_call(T&& t)
	{
		foo(std::forward<T>(t));
	}

	void call(const A& a)
	{
		foo(a);
		template_call(a);
	}

	void mall(A&& a)
	{
		foo(std::move(a));
		template_call(std::move(a));
		foo(std::forward<A>(a));
		template_call(std::forward<A>(a));
	}
}

int main(int, char**)
{
	A a;
	call(a);
	mall(std::move(a));

	{
		const auto result{decayed_as<A, const A&>};
		cout << "decayed_as: A vs. const A&: " << boolalpha << result << endl;
		if (!result)
			return 1;
	}

	{
		const auto result{decayed_as<const A&, A&&>};
		cout << "decayed_as: const A& vs. A&&: " << boolalpha << result << endl;
		if (!result)
			return 2;
	}

	{
		const auto result{decayed_as<A, B>};
		cout << "decayed_as: A vs. B: " << boolalpha << result << endl;
		if (result)
			return 3;
	}

	{
		const auto result{cvref_of<const A&, A>};
		cout << "cvref_of: const A&, A: " << boolalpha << result << endl;
		if (!result)
			return 4;
	}

	{
		const auto result{cvref_of<A&&, A>};
		cout << "cvref_of: A&&, A: " << boolalpha << result << endl;
		if (!result)
			return 5;
	}

	{
		const auto result{cvref_of<B, A>};
		cout << "cvref_of: A, B: " << boolalpha << result << endl;
		if (result)
			return 6;
	}

	return 0;
}
