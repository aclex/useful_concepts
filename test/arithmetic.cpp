#include <iostream>

#include <useful_concepts/concepts.h>

using namespace std;

int main(int, char**)
{
	cout << "int is arithmetic: " << useful_concepts::arithmetic<int> << endl;
	cout << "double is arithmetic: " << useful_concepts::arithmetic<double> << endl;
	cout << "string is arithmetic: " << useful_concepts::arithmetic<string> << endl;

	return 0;
}
