macro(create_test test_name)
	add_executable(${test_name} ${test_name}.cpp)
	target_link_libraries(${test_name} PRIVATE ${PROJECT_NAME})
	target_include_directories(${test_name} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
	add_test(${test_name} ${test_name})
endmacro()

create_test(arithmetic)
create_test(basic_usage)
